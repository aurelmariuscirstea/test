@extends('layouts.app')
@section('content')
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/test.css') }}" >
    <title>Test</title>
</head>
<body>
<div class="intro-banner">
    <div class="container" id="details-content">
        <div class="title">
            <h1>Curabitur lobortis id lorem id bibendum.</h1>
        </div>
        <div class="featured">
            <p>Nam dapibus nisl vitae elit fringilla rutrum. Aenean sollicitudin, erat a elementum.</p>
        </div>
    </div>
</div>

<div class="container">
    <div class="heading-spacer"></div>
    <h1 id="details-page">Lorem ipsum dolor</h1>
</div>
@if((Auth::user()->id) == '1')
<div class="container">
    <div class="row" >
        @foreach($menus as $menu)
        <div class="col-sm-4">
            <img class="img-responsive" src="{{$menu->picture}}">
            <h1 class="milestone">{{$menu->category}}</h1>
            <div class="title">
                <h1>{{$menu->title}}</h1>
            </div>
            <div class="featured">
                <p>{{$menu->description}}</p>
            </div>
        </div>
        @endforeach
    </div>
</div>
@else
<div class="container">
    <div class="row" >
        <div class="col-sm-12">
            <h1 style="color: red"><i>Please login<i></i></h1>
        </div>
    </div>
</div>
@endif
<div class="container">
    <div class="row" >
        <div class="col-sm-6">
            <img class="img-responsive" src="{{asset('images/g-7.jpg')}}">
            <h1 class="milestone">Milestone</h1>
            <div class="title">
                <h1>Organizing the market to deliver smart solutions</h1>
            </div>
            <div class="featured">
                <p>Given the rapid development of technology that drives Internet Of Things we see a strong need for organizations to structure the supply...</p>
            </div>
        </div>
        <div class="col-sm-6">
            <img class="img-responsive" src="{{asset('images/xsltnmf0_c.jpg')}}">
            <h1 class="milestone">Milestone</h1>
            <div class="title">
                <h1>Smart data services to help you make critical decisions</h1>
            </div>
            <div class="featured">
                <p>We believe that the true value of Internet Of Things is in processing data into valuable information. Considering it as our core focus…</p>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid">
    <div class="container">
        <h1>Check how we can kickstart your smart projects</h1>
        <span><img src="{{asset('images/right-arrow.png')}}"></span>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-sm-8">
            <img src="{{asset('images/04bb821.png')}}">
            <h1>refolow</h1>
        </div>
        <div class="col-sm-4">
            <div class="col-sm-6">
                <h1>Info</h1>
                <p>Applications</p>
                <p>Products</p>
                <p>About us</p>
            </div>
            <div class="col-sm-6">
                <h1>Offices</h1>
                <p>Netherlands</p>
                <p>Dubai</p>
            </div>
        </div>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <div class="col-sm-8">
            <p>(c) Reflow IT 2017</p>
        </div>
        <div class="col-sm-4">
            <div class="col-sm-6">
                <h1>Terms and conditions</h1>
            </div>
            <div class="col-sm-6">
                <h1>Privacy policy</h1>
            </div>
        </div>
    </div>
</footer>
</body>
</html>
@endsection