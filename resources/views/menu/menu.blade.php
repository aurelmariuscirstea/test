@extends('layouts.app')
@section('content')
<meta name="_token" content="{!! csrf_token() !!}" />
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<div class="container">
    <div class="row">
        <div class="col-sm-4">
        </div>
        <div class="col-sm-4">
            <form id="frmItems" name="frmItems" method="post" class="form-horizontal" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="form-group error">
                    <label>Category</label>
                    <input type="text" class="form-control" id="category" name="category" placeholder="Category:" value="">
                </div>

                <div class="form-group">
                    <label>Title</label>
                    <input type="text" class="form-control" id="title" name="title" placeholder="Title:" value="">
                </div>

                <div class="form-group">
                    <label>Description</label>
                    <input type="text" class="form-control" id="description" name="description" placeholder="Description:" value="">
                </div>

                <div class="form-group">
                    <label>Picture</label>
                    <input type="file" class="form-control" id="picture" name="picture" placeholder="Picture:" value="">
                </div>

                <div class="form-group">
                    <button type="button" class="btn btn-primary" id="btn-save">Save</button>
                </div>
            </form>
            <input type="hidden" id="item_id" name="item_id" value="0">
        </div>
        <div class="col-sm-4">
        </div>
    </div>
</div>
<script>
    $(document).ready(function(){

        var url = "/project_roweb_test/public/menu/items";
        $(document).on('click','#btn-save',function () {

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            })

            var formData = $("form").serialize('#frmItems');

            console.log(formData);

            $.ajax({
                type: "POST",
                url: url,
                data: formData,
                dataType: 'json',
                success: function (data) {
                    console.log(data);

                    $('#frmItems').trigger("reset");

                },
                error: function (data) {
                    console.log('Error:', data);
                }
            });
        });
    });
</script>
@endsection