<?php
/**
 * Created by IntelliJ IDEA.
 * User: stude
 * Date: 24.11.2017
 * Time: 15:01
 */

namespace App;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    protected $fillable = ['category', 'title','description','picture','id_user'];
}


