<?php
/**
 * Created by IntelliJ IDEA.
 * User: stude
 * Date: 24.11.2017
 * Time: 14:29
 */

namespace App\Http\Controllers;

use App\Menu;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;

class MenuController
{
    public function index()
    {
        return view('menu.menu');
    }

    public function add(Request $request)
    {
        $id_user = Auth::user()->id;
        return Input::file('picture');

            $filename = Input::file('picture')->getClientOriginalName();
            $file = Input::file('picture')->move(public_path('images'), $filename);

            $item = Menu::create([
                'category' => $request->category,
                'title' => $request->title,
                'description' => $request->description,
                'picture' => $file,
                'id_user' => $id_user,
            ]);
            return response()->json(['success'=>$item]);

    }

}